package model.logic;

import api.ITaxiTripsManager;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import model.vo.Taxi;
import model.vo.Service;
import model.data_structures.LinkedList;
import model.data_structures.MyIterator;
import model.data_structures.MyList;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 
	private MyList<Service> services;

	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub
		services = new MyList<Service>();
		System.out.println("Inside loadServices with " + serviceFile);
		JsonParser parser = new JsonParser();
		try{
			JsonArray arr= (JsonArray) parser.parse(new FileReader(serviceFile));
			for(int i = 0; arr != null && i < arr.size(); i++){
				JsonObject obj= (JsonObject) arr.get(i);
				System.out.println("------------------------------------------------------------------------------------------------");
				System.out.println(obj);

				String company = "NaN";
				if ( obj.get("company") != null )
				{ company = obj.get("company").getAsString(); }

				String taxiId = "NaN";
				if ( obj.get("taxi_id") != null )
				{ taxiId = obj.get("taxi_id").getAsString(); }

				Taxi taxi = new Taxi(taxiId, company);

				String doCensus = "NaN";
				if ( obj.get("dropoff_census_tract") != null )
				{ doCensus = obj.get("dropoff_census_tract").getAsString(); }

				String doLatitude = "NaN";
				if ( obj.get("dropoff_centroid_latitude") != null )
				{ doLatitude = obj.get("dropoff_centroid_latitude").getAsString(); }

				String doLongitude = "NaN";
				if ( obj.get("dropoff_centroid_longitude") != null )
				{ doLongitude = obj.get("dropoff_centroid_longitude").getAsString(); }

				int doCommunity = 0;
				if ( obj.get("dropoff_community_area") != null )
				{ doCommunity = Integer.parseInt(obj.get("dropoff_community_area").getAsString()); }

				double extras = 0;
				if ( obj.get("extras") != null )
				{ extras = Double.parseDouble(obj.get("extras").getAsString()); }

				double fare = 0;
				if ( obj.get("fare") != null )
				{ fare = Double.parseDouble(obj.get("fare").getAsString()); }

				String paymentType = "NaN";
				if ( obj.get("payment_type") != null )
				{ doLongitude = obj.get("payment_type").getAsString(); }

				String puCensus = "NaN";
				if ( obj.get("pickup_census_tract") != null )
				{ puCensus = obj.get("pickup_census_tract").getAsString(); }

				String puLatitude = "NaN";
				if ( obj.get("pickup_centroid_latitude") != null )
				{ puLatitude = obj.get("pickup_centroid_latitude").getAsString(); }

				String puLongitude = "NaN";
				if ( obj.get("pickup_centroid_longitude") != null )
				{ puLongitude = obj.get("pickup_centroid_longitude").getAsString(); }

				int puCommunity = 0;
				if ( obj.get("pickup_community_area") != null )
				{ puCommunity = Integer.parseInt(obj.get("pickup_community_area").getAsString()); }

				double tips = 0;
				if ( obj.get("tips") != null )
				{ tips = Double.parseDouble(obj.get("tips").getAsString()); }

				double tolls = 0;
				if ( obj.get("tolls") != null )
				{ tolls = Double.parseDouble(obj.get("tolls").getAsString()); }

				String tripEnd = "NaN";
				if ( obj.get("trip_end_timestamp") != null )
				{ tripEnd = obj.get("trip_end_timestamp").getAsString(); }

				String tripId = "NaN";
				if ( obj.get("trip_id") != null )
				{ tripId = obj.get("trip_id").getAsString(); }

				int seconds = 0;
				if ( obj.get("trip_seconds") != null )
				{ seconds = Integer.parseInt(obj.get("trip_seconds").getAsString()); }

				double miles = 0;
				if ( obj.get("trip_miles") != null )
				{ miles = Double.parseDouble(obj.get("trip_miles").getAsString()); }

				String tripStart = "NaN";
				if ( obj.get("trip_start_timestamp") != null )
				{ tripStart = obj.get("trip_start_timestamp").getAsString(); }

				double total = 0;
				if ( obj.get("trip_total") != null )
				{ total = Double.parseDouble(obj.get("trip_total").getAsString()); }

				Service toAdd = new Service(taxi, tripId, seconds, miles, total, doCensus, doLongitude, doLatitude, doCommunity, extras, fare, paymentType, puCensus, puLongitude, puLatitude, puCommunity, tips, tolls, tripEnd, tripStart);
				services.add(toAdd);
			}
		}
		catch (JsonIOException e1 ) {
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			e3.printStackTrace();
		}
	}

	@Override
	public LinkedList<Taxi> getTaxisOfCompany(String company) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxisOfCompany with " + company);
		System.out.println(services.get(0).getTaxi().getCompany());
		MyList<Taxi> taxis = new MyList<Taxi>();

		if(services!=null){
			MyIterator<Service> iter = new MyIterator<Service>(services.getNode(0));

			if(iter.getCurrent() != null){
				while(iter.hasNext()){
					if(iter.getCurrent().getTaxi().getCompany().compareTo(company) == 0){
						Taxi toAdd = iter.getCurrent().getTaxi();
						taxis.add(toAdd);
					}
					iter.next();
				}
			}
		}
		return taxis;
	}

	@Override
	public LinkedList<Service> getTaxiServicesToCommunityArea(int communityArea) {
		// TODO Auto-generated method stub
		System.out.println("Inside getTaxiServicesToCommunityArea with " + communityArea);
		MyList<Service> services1 = new MyList<Service>();
		if(services!=null){
			MyIterator<Service> iter = new MyIterator<Service>(services.getNode(0));

			if(iter.getCurrent() != null){
				while(iter.hasNext()){
					if(iter.getCurrent().getPickUpCommunityArea() == communityArea){
						Service toAdd = iter.getCurrent();
						services1.add(toAdd);
					}
					iter.next();
				}
			}
		}
		return services1;
	}


}
