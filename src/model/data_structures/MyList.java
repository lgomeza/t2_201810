package model.data_structures;

public class MyList<T extends Comparable<T>> implements LinkedList<T> {

	private Node<T> first;
	
	private int numberOfElements;
	
	 /**
     * Constructs an empty list.
     * <b>post:< /b> The first node has been initialized in null
     */
	public MyList() 
	{
		first = null;
		numberOfElements  = 0;
	}
	
	/**
     * A list is constructed where the first node will have the first element.
     * @param nPrimero el elemento a guardar en el primer nodo
     * @throws NullPointerException si el elemento recibido es nulo
     */
	public MyList(T pFirst)
	{
		first = new Node<T>(pFirst);
		numberOfElements = 1;
	}
	
	@Override
	public boolean add(T element) {
		// TODO Auto-generated method stub
		if(element == null){
			throw new NullPointerException("The element added is null");
		}
		boolean correctlyAdded = false;
		if(!contains(element)){
			Node<T> toAdd = new Node<T>(element);
			if(first == null){
				first = toAdd;
				numberOfElements ++;
				correctlyAdded = true;
			}
			else{
				Node<T> last = getNode(size() - 1);
				last.changeNext(toAdd);
				toAdd.changePrevious(last);
				toAdd.changeNext(null);
				numberOfElements ++;
				correctlyAdded = true;
			}
		}
		return correctlyAdded;
	}

	@Override
	public boolean delete(T element) {
		// TODO Auto-generated method stub
		boolean deleted = false;
		if(contains(element)){
			
			int index = indexOf(element);
			
			if(index == 0){
				first = first.getNext();
				first.changePrevious(null);
				numberOfElements--;
				deleted = true;
			}
			else if(index == size()-1){
				Node<T> previous = getNode(index-1);
				previous.changeNext(null);
				numberOfElements--;
				deleted = true;
			}
			else{
				Node<T> previous = getNode(index-1);
				Node<T> actual = getNode(index);
				Node<T> next = actual.getNext();
				previous.changeNext(next);;
				next.changePrevious(previous);;
				numberOfElements--;
				deleted = true;
			}
			
		}
		return deleted;
	}

	@Override
	public T get(T element) {
		// TODO Auto-generated method stub
		T toGet = null;
		if(contains(toGet)){
			int index = indexOf(toGet);
			toGet = getNode(index).getElement();
		}
		return toGet;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return numberOfElements;
	}

	@Override
	public T get(int index) {
		// TODO Auto-generated method stub
		if(index<0 || index >= size()){
			throw new IndexOutOfBoundsException("The object is out of the Array's bounds");
		}
		
		Node<T> elementNode = getNode(index);
		T element = elementNode.getElement();
		
		return element;
	}

	

	/**
     * Indicates if the list has the element given by parameter
     * @param element The element you want to find in the list. element != null
     * @return true if the object is in the list, false if it isn't.
     */
	public boolean contains(T element) 
	{
		boolean contains = true;
		if(indexOf(element) == -1){
			contains = false;
		}
		
		return contains;
	}
	
	/**
     * Indicates the first position in the list of the object given by parameter
     * @param element The element you want to find in the list. element != null
     * @return The object's position or -1 in case it's not in the list.
     */
	public int indexOf(T element) 
	{
		int posObj = -1;
		int pos = 0;
		Node<T> actual = first;
		boolean found = false;
		while(actual != null && !found){
			if(actual.getElement().hashCode() == element.hashCode()){
				posObj = pos;
				found = true;
			}
			actual = actual.getNext();
			pos++;
		}
		return posObj;
	}
	
	/**
     * Returns the node on the position given by parameter
     * @param index the position given.
     * @return The node on the position given by parameter.
     * @throws IndexOutOfBoundsException if index < 0 o index >= size()
     */
	public Node<T> getNode(int index) throws IndexOutOfBoundsException
	{
		if(index < 0 || index > numberOfElements)
		{
			throw new IndexOutOfBoundsException("You are asking for the index: " + index + " and the list has a total of " + numberOfElements +" elements");
		}
		
		Node<T> actual = first;
		int posActual = 0;
		while(actual != null && posActual < index)
		{
			actual = actual.getNext();
			posActual ++;
		}
		
		return actual;
	}
	
	
	//Maybe it is better to use them on a separate iterator.
	@Override
	public void listing() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public T getCurrent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
		return null;
	}
}
