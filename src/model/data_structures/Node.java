package model.data_structures;

public class Node<T extends Comparable<T>> {
	
	/**
	 * Element saved on the Node.
	 */
	private T element;
	
	/**
	 * Previous node.
	 */
	private Node<T> previous;
	
	/**
	 * Next node.
	 */
	private Node<T> next;
	
	/**
	 * Node's constructor.
	 * @param element El elemento que se almacenará en el nodo. elemento != null
	 */
	public Node(T element)
	{
		this.element = element;
		previous = null;
		next = null;
	}
	
	/**
	 * Returns the element contained in the node.
	 * @return The element contained in the node.
	 */
	public T getElement(){
		return element;
	}
	
	/**
	 * Returns the previous node.
	 * @return the previous node.
	 */
	public Node<T> getPrevious(){
		return previous;
	}
	
	/**
	 * Returns the next node.
	 * @return the next node.
	 */
	public Node<T> getNext(){
		return next;
	}
	
	/**
	 * Changes the node's element for the one received by parameter.
	 * @param element The new element of the node.
	 */
	public void changeElement(T element){
		this.element = element;
	}
	
	/**
	 * Changes the previous node for the one received by parameter.
	 * @param element The new previous node of the node.
	 */
	public void changePrevious(Node<T> previous){
		this.previous = previous;
	}
	
	/**
	 * Changes the next node for the one received by parameter.
	 * @param element The next previous node of the node.
	 */
	public void changeNext(Node<T> next){
		this.next = next;
	}
	
	public int getIdentifier(){
		return element.hashCode();
	}
	
}
