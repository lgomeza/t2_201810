package model.data_structures;

import java.util.ListIterator;

public class MyIterator<T extends Comparable<T>> implements ListIterator<T> {
	
	private Node<T> actual;
	
	public MyIterator(Node<T> firstNode){
		actual = firstNode;
	}
	
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return actual != null;
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
		T element = actual.getElement();
		actual = actual.getNext();
		return element;
	}

	@Override
	public boolean hasPrevious() {
		// TODO Auto-generated method stub
		return actual.getPrevious() != null;
	}

	@Override
	public T previous() {
		T element = actual.getPrevious().getElement();
		actual = actual.getPrevious();
		return element;
	}
	
	/**
	 * Sets the listing of elements at the first element.
	 */
	public void listing(){
		while(hasPrevious()){
			previous();
		}
	}
	
	/**
	 * Returns the current element T in the listing
	 * @return Null if it doesn't exist.
	 */
	public T getCurrent(){
		return actual.getElement();
	}
	
	
	
	//not going to need these.
	@Override
	public int nextIndex() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int previousIndex() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void set(T e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void add(T e) {
		// TODO Auto-generated method stub
		
	}

}
