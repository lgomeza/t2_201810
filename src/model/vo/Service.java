package model.vo;

import java.util.Date;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {
	
	private Taxi taxi;
	
	private String tripId;
	
	private int tripSeconds;
	
	private double tripMiles;
	
	private double tripTotal;
	
	private String doCensus;
	
	private String doLongitude;
	
	private String doLatitude;
	
	private int doCommunityArea;
	
	private double extras;
	
	private double fare;
	
	private String paymentType;
	
	private String puCensus;
	
	private String puLongitude;
	
	private String puLatitude;
	
	private int puCommunityArea;
	
	private double tips;
	
	private double tolls;
	
	private String tripEnd;
	
	private String tripStart;

	
	public Service( Taxi taxi, String tripId, int seconds, double miles, double total, String doCensus,
			String doLongitude, String doLatitude, int doCommunity, double extras, double fare, String paymentType,
			String puCensus, String puLongitude, String puLatitude, int puCommunity, double tips, double tolls,
			String tripEnd, String tripStart){

		this.taxi = taxi;
		this.tripId = tripId;
		this.tripSeconds = seconds;
		this.tripMiles = miles;
		this.doCensus = doCensus;
		this.doLongitude = doLongitude;
		this.doLatitude = doLatitude;
		this.doCommunityArea = doCommunity;
		this.extras = extras;
		this.fare = fare;
		this.paymentType = paymentType;
		this.puCensus = doCensus;
		this.puLongitude = doLongitude;
		this.puLatitude = doLatitude;
		this.puCommunityArea = puCommunity;
		this.tips = tips;
		this.tolls = tolls;
		this.tripEnd = tripEnd;
		this.tripStart = tripStart;
		
	}
	
	public Taxi getTaxi(){
		return taxi;
	}
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return tripId;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi.getTaxiId();
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSeconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return tripTotal;
	}
	
	public int getPickUpCommunityArea(){
		return puCommunityArea;
	}
	
	
	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public String toString(){
		return "{\"company\":\"" + taxi.getCompany() + "\", \"dropoff_census_tract\":\"" + doCensus + "\", \"dropoff_centroid_latitude\":\"" + doLatitude +
				"\", \"dropoff_centroid_longitude\":\"" + doLongitude + "\", \"dropoff_community_area\":\"" + doCommunityArea + "\", \"extras\":\"" + extras + 
				"\", \"fare\":\""+ fare + "\", \"payment_type\":\"" + paymentType + "\", \"pickup_census_tract\":\"" + puCensus + "\", \"pickup_centroid_latitude\":\"" + puLatitude +
				"\", \"pickup_centroid_longitude\":\"" + puLongitude + "\", \"pickup_community_area\":\"" + puCommunityArea + "\", \"taxi_id\":\"" + taxi.getTaxiId() + 
				"\", \"tips\":\"" + tips + "\", \"tolls\":\""+ tolls + "\", \"trip_end_timestamp\":\"" + tripEnd + "\", \"trip_id\":\"" + tripId + 
				"\", \"trip_miles\":\"" + tripMiles + "\", \"tripSeconds\":\"" + tripSeconds + "\", \"trip_start_timestamp\":\"" + tripStart + "\", \"trip_total\":\"" + tripTotal;
	}
}
