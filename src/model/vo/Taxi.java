package model.vo;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	private String id;
	
	private String company;
	
	public Taxi(String pId, String pCompany){
		id = pId;
		company = pCompany;
	}
	
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return id;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		return 0;
	}	
	
	public String toString(){
		return "\"company\":\""+ company + "\", \"taxi_id\": " + id;
	}
}
